<?php include 'layout/header.html'; ?>

    <!-- FORMULARIO -->
    <div class="container" style="margin-top:100px;">
        <div class="panel panel-default">
            <div class="panel-heading">Formulario de Cadastro</div>
            <?php
            if(!empty($_GET['error'])){
                if($_GET['error'] == 1){ ?>
                    <div class="alert alert-danger">CPF INVALIDO</div>
                <?php }
            } ?>
            <div class="panel-body">
                <?php
                    include 'db/database.php';
                    $id = $_GET['id'];
                    $db = $conn->query("SELECT * FROM CRUD WHERE Id = $id ");
                    $linha = $db->fetch(PDO::FETCH_ASSOC);

                 ?>
                <form action="update.php?id=<?php echo $linha['Id']; ?>" method="post">

                  <div class="form-group">
                    <label for="nome">Nome:</label>
                    <input type="text" class="form-control" value="<?php echo $linha['Nome']; ?>" name="nome" id="nome" placeholder="Nome">
                  </div>
                  <div class="form-group">
                    <label for="tel">Telefone</label>
                    <input type="text" class="form-control" value="<?php echo $linha['Telefone']; ?>" name="tel" id="tel" placeholder="Telefone">
                  </div>

                  <div class="form-group">
                    <label for="dm">Data de Nascimento</label>
                    <input type="text" class="form-control" value="<?php echo $linha['DataDeNascimento']; ?>" name="dm" id="dm" placeholder="Data de Nascimento">
                  </div>

                  <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" class="form-control" value="<?php echo $linha['Email']; ?>" name="email" id="email" placeholder="Email">
                  </div>

                  <div class="form-group">
                    <label for="peso">Peso</label>
                    <input type="number" step="0.2" class="form-control" value="<?php echo $linha['Peso']; ?>" name="peso" id="peso" placeholder="Peso">
                  </div>

                  <div class="form-group">
                    <label for="cpf">CPF</label>
                    <input type="text" class="form-control" value="<?php echo $linha['CPF']; ?>" name="cpf" id="cpf" placeholder="Cpf">
                  </div>

                  <div class="form-group">
                    <label for="senha">Senha</label>
                    <input type="password" class="form-control" value="<?php echo $linha['Senha']; ?> " name="senha" id="senha" placeholder="Senha">
                  </div>
                  <div class="form-group">
                    <label for="obs">Observação</label>
                    <input type="text" class="form-control" value="<?php echo $linha['Obs']; ?>" name="obs" id="obs" placeholder="Observação">
                  </div>

                  <button type="submit" class="btn btn-default">Submit</button>
               </form>
            </div>
        </div>
      </div>
    </div>
    
<?php include 'layout/footer.html'; ?>
