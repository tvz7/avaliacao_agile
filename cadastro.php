<?php include 'layout/header.html'; ?>
    <!-- FORMULARIO -->
    <div class="container" style="margin-top:100px;">
        <div class="panel panel-default">
            <div class="panel-heading">Formulario de Cadastro</div>

            <?php
            if(!empty($_GET['successC'])){
                if($_GET['successC'] == 1){ ?>
                    <div class="alert alert-success">Inserido com sucesso!</div>
        <?php   } else if ($_GET['successC'] == 0) { ?>
                    <div class="alert alert-danger">Falha!</div>
            <?php }
            }    ?>

            <?php
            if(!empty($_GET['error'])){
                if($_GET['error'] == 1){ ?>
                    <div class="alert alert-danger">Respostas invalidas, preecha todos os campos corretamente</div>
        <?php   }
            } ?>
            <div class="panel-body">
                <form action="create.php" method="post">
                  <div class="form-group">
                    <label for="nome">Nome:</label>
                    <input type="text" class="form-control" name="nome" id="nome" placeholder="Nome" required>
                  </div>
                  <div class="form-group">
                    <label for="tel">Telefone</label>
                    <input type="text" class="form-control" name="tel" id="tel" placeholder="Telefone" required>
                  </div>

                  <div class="form-group">
                    <label for="calendario">Data de Nascimento</label>
                    <input type="text" name="dm" class="form-control"id="calendario" placeholder="Data de Nascimento" maxlength="10" onkeypress="mascaraData(this)" required>
                  </div>

                  <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" class="form-control" name="email" id="email" placeholder="Email" required>
                  </div>

                  <div class="form-group">
                    <label for="peso">Peso</label>
                    <input type="number" step="0.2" class="form-control" name="peso" id="peso" placeholder="Peso" required>
                  </div>

                  <div class="form-group">
                    <label for="cpf">CPF</label>
                    <input type="text" class="form-control" name="cpf" id="cpf" placeholder="Cpf" required>
                  </div>

                  <div class="form-group">
                    <label for="senha">Senha</label>
                    <input type="password" class="form-control" name="senha" id="senha" placeholder="Senha" required>
                  </div>
                  <div class="form-group">
                    <label for="obs">Observação</label>
                    <input type="text" class="form-control" name="obs" id="obs" placeholder="Observação" required>
                  </div>

                  <button type="submit" class="btn btn-default">Submit</button>
               </form>
            </div>
        </div>
      </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="http://code.jquery.com/jquery-1.8.2.js"></script>
    <script src="http://code.jquery.com/ui/1.9.0/jquery-ui.js"></script>
    <script>
        $(function() {
            $("#calendario").datepicker({
                dateFormat: 'yy-mm-dd',
                changeMonth: true,
                changeYear: true

            });
        });
    </script>
    </script>
  </body>
</html>
