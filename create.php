<?php

include 'db/database.php';

$nome = $_POST['nome'];
$telefone = $_POST['tel'];
$dm = $_POST['dm'];
$email = $_POST['email'];
$peso = $_POST['peso'];
$cpf =  $_POST['cpf'];
$senha =  md5($_POST['senha']);
$obs =  $_POST['obs'];
$data = date("Y/m/d");
$dataCadastro = $data;


function validarCPF( $cpf = '' ) {

	$cpf = str_pad(preg_replace('/[^0-9]/', '', $cpf), 11, '0', STR_PAD_LEFT);
	// todos esses são falsos
	if ( strlen($cpf) != 11 || $cpf == '00000000000' || $cpf == '11111111111' || $cpf == '22222222222' || $cpf == '33333333333' || $cpf == '44444444444' || $cpf == '55555555555' || $cpf == '66666666666' || $cpf == '77777777777' || $cpf == '88888888888' || $cpf == '99999999999') {
		return FALSE;
	} else {
		for ($t = 9; $t < 11; $t++) {
			for ($d = 0, $c = 0; $c < $t; $c++) {
				$d += $cpf{$c} * (($t + 1) - $c);
			}
			$d = ((10 * $d) % 11) % 10;
			if ($cpf{$c} != $d) {
				return FALSE;
			}
		}
		return TRUE;
	}
}

if(validarCPF($cpf) && is_numeric($telefone) && $peso > 0 ){

    $db = $conn->prepare("INSERT INTO CRUD (Nome, Telefone, Email, DataDeCadastro, DataDeNascimento, Peso, CPF, Senha, Obs) VALUES (?,?,?,?,?,?,?,?,?)");

    $db->bindValue(1, $nome);
    $db->bindValue(2, $telefone);
    $db->bindValue(3, $email);
    $db->bindValue(4, $dataCadastro);
    $db->bindValue(5, $dm);
    $db->bindValue(6, $peso);
    $db->bindValue(7, $cpf);
    $db->bindValue(8, $senha);
    $db->bindValue(9, $obs);
    $db->execute();

    if($db){

        header("Location: cadastro.php?successC=1");

    } else {
        header("Location: cadastro.php?successC=0");
    }
} else {
    header("Location: cadastro.php?error=1");
}


 ?>
