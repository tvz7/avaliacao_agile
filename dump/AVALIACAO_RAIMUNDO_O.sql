-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 11, 2017 at 02:50 PM
-- Server version: 5.7.17-0ubuntu0.16.04.1
-- PHP Version: 7.0.15-0ubuntu0.16.04.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `AVALIACAO_RAIMUNDO_O`
--

-- --------------------------------------------------------

--
-- Table structure for table `CRUD`
--

CREATE TABLE `CRUD` (
  `Id` int(11) NOT NULL,
  `Nome` varchar(55) NOT NULL,
  `Telefone` varchar(15) NOT NULL,
  `Email` varchar(30) NOT NULL,
  `DataDeCadastro` date NOT NULL,
  `DataDeNascimento` date NOT NULL,
  `Peso` float NOT NULL,
  `CPF` varchar(11) NOT NULL,
  `Senha` varchar(55) NOT NULL,
  `Obs` mediumtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `CRUD`
--

INSERT INTO `CRUD` (`Id`, `Nome`, `Telefone`, `Email`, `DataDeCadastro`, `DataDeNascimento`, `Peso`, `CPF`, `Senha`, `Obs`) VALUES
(22, 'Clarice', '987654132654', 'taviinho.correia7@hotmail.com', '2017-03-11', '2017-03-01', 60, '07765965514', '71e2f2048658ce464d214d786e0a1582', 'LInda');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `CRUD`
--
ALTER TABLE `CRUD`
  ADD PRIMARY KEY (`Id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `CRUD`
--
ALTER TABLE `CRUD`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
