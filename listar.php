<?php include 'layout/header.html'; ?>


        <div class="container" style="margin-top:100px;">
          <h2>Listagem de clientes</h2>
          <?php
                if(!empty($_GET['success'])){
                    if($_GET['success'] == 1){ ?>
                        <div class="alert alert-success">Deletado com sucesso!</div>
            <?php   } else if ($_GET['success'] == 0) { ?>
                        <div class="alert alert-danger">Falha!</div>
                <?php }
                } ?>

          <?php
              if(!empty($_GET['successU'])){
                    if(array_key_exists('successU', $_GET) && ($_GET['successU'] == 1)){ ?>
                        <div class="alert alert-success">Atualizado Com Sucesso</div>
            <?php    } else if ($_GET['successU'] == 0) { ?>
                        <div class="alert alert-danger">Falha!</div>
                <?php }
                } ?>
                <div class="table-responsive">
                    <table class="table">
                      <thead>
                        <tr>
                          <th>ID</th>
                          <th>Nome</th>
                          <th>Email</th>
                          <th>Telefone</th>
                          <th>DataDeCadastro</th>
                          <th>DataDeNascimento</th>
                          <th>Peso</th>
                          <th>CPF</th>
                          <th>OBS</th>
                          <th>Ações</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php

                          include 'db/database.php';
                          include 'read.php';
                              foreach ($linha as $pessoa) { ?>

                                  <tr>
                                          <td><?php echo $pessoa['Id'] ?></td>
                                          <td><?php echo $pessoa['Nome'] ?></td>
                                          <td><?php echo $pessoa['Email'] ?></td>
                                          <td><?php echo $pessoa['Telefone'] ?></td>
                                          <td><?php echo $pessoa['DataDeCadastro'] ?></td>
                                          <td><?php echo $pessoa['DataDeNascimento'] ?></td>
                                          <td><?php echo $pessoa['Peso'] ?></td>
                                          <td><?php echo $pessoa['CPF'] ?></td>
                                          <td><?php echo $pessoa['Obs'] ?></td>
                                          <td><a href="delete.php?id=<?php echo $pessoa['Id'] ?>" class="btn btn-danger glyphicon glyphicon-trash"></a></td>
                                          <td><a href="atualizar.php?id=<?php echo $pessoa['Id'] ?>" class="btn btn-primary glyphicon glyphicon-refresh"></a></td>

                                  </tr>

                      <?php }  ?>
                     </tbody>

                    </table>
                </div>
        </div>
<?php include 'layout/footer.html'; ?>
